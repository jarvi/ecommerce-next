import { useState, useEffect } from "react"
import { Container, Menu, Grid, Icon, Label, Button } from "semantic-ui-react"
import Link from "next/link"
import BasicModal from "../../Modal/BasicModal"
import Auth from "../../Auth"
import useAuth from "../../../hooks/useAuth"
import { getMeApi } from "../../../api/user"
import { getPlatformsApi } from "../../../api/platform"
import useCart from "../../../hooks/useCart"

export default function MenuWeb() {
  const [platforms, setPlatform] = useState([])
  const [showModal, setShowModal] = useState(false)
  const [titleModal, setTitleModal] = useState("Iniciar")
  const [user, setUser] = useState(undefined)
  const { logout, auth } = useAuth()

  useEffect(() => {
    ;(async () => {
      const response = await getMeApi(logout)
      setUser(response)
    })()
  }, [auth])

  useEffect(() => {
    ;(async () => {
      const response = await getPlatformsApi()
      setPlatform(response || [])
    })()
  }, [])

  const onShowModal = () => {
    setShowModal(true)
  }

  const onCloseModal = () => setShowModal(false)

  return (
    <div className="menu">
      <Container>
        <Grid>
          <Grid.Column className="menu__left" width={6}>
            <MenuPlatforms platforms={platforms} />
          </Grid.Column>
          <Grid.Column className="menu__right" width={10}>
            {user !== undefined && (
              <MenuOptions on={onShowModal} user={user} logout={logout} />
            )}
          </Grid.Column>
        </Grid>
      </Container>
      <BasicModal
        show={showModal}
        setShow={setShowModal}
        title={titleModal}
        size="small"
      >
        <Auth onCloseModal={onCloseModal} setTitleModal={setTitleModal} />
      </BasicModal>
    </div>
  )
}

function MenuPlatforms({ platforms }) {
  return (
    <Menu>
      {platforms.map(p => (
        <Link href={"/games/" + p.url} key={p._id}>
          <Menu.Item as="a" name={p.url}>
            {p.title}
          </Menu.Item>
        </Link>
      ))}
    </Menu>
  )
}

function MenuOptions({ on, user, logout }) {
  //const { on: x } = props
  const { productsCard } = useCart()
  return (
    <Menu>
      {user ? (
        <>
          <Link href="/ordersPage">
            <Menu.Item as="a">
              <Icon name="game" />
              Mis pedidos
            </Menu.Item>
          </Link>
          {/* <Link href="/wishlist">
            <Menu.Item as="a">
              <Icon name="heart outline" />
              Wishlist
            </Menu.Item>
          </Link> */}
          <Link href="/accountPage">
            <Menu.Item as="a">
              <Icon name="user outline" />
              {user.name} {user.lastname}
            </Menu.Item>
          </Link>
          <Link href="/cartPage">
            <Menu.Item as="a" className="m-0">
              <Icon name="cart" />
              {productsCard > 0 && (
                <Label color="red" floating circular>
                  {productsCard}
                </Label>
              )}
            </Menu.Item>
          </Link>
          <Menu.Item className="m-0" onClick={logout}>
            <Icon name="power off" />
          </Menu.Item>
        </>
      ) : (
        <Menu.Item onClick={on}>
          <Icon name="user outline" />
          Mi cuenta
        </Menu.Item>
      )}
    </Menu>
  )
}
