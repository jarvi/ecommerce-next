import { BASE_PATH } from "../utils/constants"
import { authFetch } from "../utils/fetch"

export async function getOrdersApi(idUser, logout) {

  try {
    const url = `${BASE_PATH}/orders?_sort=createdAt:desc&users_permissions_user=${idUser}`
    const result = authFetch(url, null, logout)
    return result
  } catch (e) {
    console.log("en el catch")
    console.log(e)
    return null
  }

}
