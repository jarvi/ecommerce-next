import React, { useState } from "react"
import { Image, Modal } from "semantic-ui-react"
import Slider from "react-slick"

const settings = {
  className: "carousel-screenshots",
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 2,
  swipeToSlider: true,
}

export default function CarouselScreenshots(props) {
  const { title, screenshots } = props

  const [showModal, setShowModal] = useState(false)
  const [urlImage, setUrlImage] = useState(null)

  const openImagen = url => {
    setUrlImage(url)
    setShowModal(true)
  }

  return (
    <>
      <Slider {...settings}>
        {screenshots.map(screenshot => (
          <Image
            key={screenshot.id}
            src={screenshot.url}
            alt={screenshot.name}
            onClick={_ => openImagen(screenshot.url)}
          />
        ))}
      </Slider>
      <Modal open={showModal} onClose={_ => setShowModal(false)} size="large">
        <Image src={urlImage} alt={title} />
      </Modal>
    </>
  )
}
