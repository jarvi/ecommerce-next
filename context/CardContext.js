import { createContext } from "react"

const CartContect = createContext({
  productsCard: 0,
  addProductCart: () => null,
  getProductsCart: () => null,
  removeProductCart: () => null,
  removeAllProductsCart: () => null,
})


export default CartContect