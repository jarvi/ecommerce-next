import { BASE_PATH, CART } from "../utils/constants"
import { toast } from "react-toastify"
import { remove, size, includes } from "lodash"
export function getProductsCart() {
  if (typeof window !== "undefined") {
    const cart = localStorage.getItem(CART)
    return !cart ? null : cart.split(",")
  }
}

export function addProductCart(product) {
  const cart = getProductsCart()

  if (!cart) {
    localStorage.setItem(CART, product)
    toast.success("Producto agregado al carrito")
  }

  if (cart) {
    const productFound = cart.includes(product)

    if (productFound) {
      toast.warning("Este producto ya esta en el carrito")
    }

    if (!productFound) {
      cart.push(product)
      localStorage.setItem(CART, cart)
      toast.success("producto agregado correctamente ")
    }
  }
}

export function countProductCart() {
  const cart = getProductsCart()
  if (!cart) {
    return 0
  } else {
    return cart.length
  }
}

export function removeProductCart(product) {
  const cart = getProductsCart()

  remove(cart, (item) => {
    return item === product
  })

  if(size(cart) > 0) {
    localStorage.setItem(CART, cart)
  }else{
    localStorage.removeItem(CART)
  }
}
