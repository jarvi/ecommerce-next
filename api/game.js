import { BASE_PATH } from "../utils/constants"

export async function getLastGamesApi(limit) {
  try {
    const limitItems = `_limit=${limit}`
    const sortItem = "_sort=createdAt:desc"
    const url = `${BASE_PATH}/juegos?${limitItems}&${sortItem}`
    const response = await fetch(url)
    const result = await response.json()
    return result
  } catch (e) {
    console.log(e)
    return null
  }
}

export async function getGamePlatformApi(platform, limit, start) {
  try {
    const limitItems = `_limit=${limit}`
    const sortItems = `_sort=createdAt:desc`
    const startItems = `_start=${start}`
    const url = `${BASE_PATH}/juegos?platform.url=${platform}&${limitItems}&${sortItems}&${startItems}`
    const response = await fetch(url)
    const result = await response.json()
    return result
  } catch (e) {
    console.log(e)
    return null
  }
}

export async function getTotalGamesPlatformApi(platform) {
  try {
    const url = `${BASE_PATH}/juegos/count?platform.url=${platform}`
    const response = await fetch(url)
    const result = await response.json()
    return result
  } catch (e) {
    console.log(e)
    return null
  }
}

export async function getGameByUrlApi(path) {
  try {
    const url = `${BASE_PATH}/juegos?url=${path}`
    const response = await fetch(url)
    const result = await response.json()
    return result[0] ? result[0] : result
  } catch (e) {
    console.log(e)
    return null
  }
}

export async function searchGameApi(title) {
  try {
    const url = `${BASE_PATH}/juegos?_q=${title}`
    const response = await fetch(url)
    const result = response.json()
    return result
  } catch (e) {
    console.log(e)
    return null
  }
}
