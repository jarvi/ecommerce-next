import React from "react"

export default function Custom404() {
  return (
    <div>
      <h1> La pagina que busca no fue encontrada</h1>
    </div>
  )
}
