import { useState, useEffect } from "react"
import BasicLayout from "../../layouts/BasicLayout"
import { useRouter } from "next/router"
import { getGamePlatformApi, getTotalGamesPlatformApi } from "../../api/game"
import { Loader } from "semantic-ui-react"
import ListGame from "../../components/ListGames"
import Pagination from "../../components/Pagination"
import Seo from "../../components/Seo"

const limitPerPage = 10

export default function Platform() {
  const { query } = useRouter()
  const [games, setGames] = useState("")
  const [totalGames, setTotalGames] = useState([])

  console.log(query);
  const getStartItem = () => {
    const currentPages = parseInt(query.page)
    if (!query.page || currentPages === 1) return 0
    else return currentPages * limitPerPage - limitPerPage
  }

  useEffect(() => {
    ;(async () => {
      if (query.platform) {
        const response = await getGamePlatformApi(
          query.platform,
          limitPerPage,
          getStartItem()
        )
        setGames(response)
      }
    })()
  }, [query])

  useEffect(() => {
    ;(async () => {
      const response = await getTotalGamesPlatformApi(query.platform)
      setTotalGames(response)
    })()
  }, [query])

  return (
    <BasicLayout className="plataform">
      <Seo
        title="en plataforma"
        description="estas en la seccion de plataforma"
      />
      {!games && <Loader active> Cargando </Loader>}
      {games && games.length === 0 && <h3>no hay juegos para mostrar</h3>}
      {games.length > 0 && <ListGame games={games}></ListGame>}

      {totalGames ? (
        <Pagination
          totalGames={totalGames}
          page={query.page ? parseInt(query.page) : 1}
          limitPerPage={limitPerPage}
        />
      ) : (
        ""
      )}
    </BasicLayout>
  )
}
