import { useMemo, useState, useEffect } from "react"
import { useRouter } from "next/router"
import { ToastContainer, toast } from "react-toastify"
import jwtDecode from "jwt-decode"
import AuthContext from "../context/AuthContext"
import CartContext from "../context/CardContext"
import { setToken, getToken, removeToken } from "../api/token"
import {
  getProductsCart,
  addProductCart,
  countProductCart,
  removeProductCart,
} from "../api/cart"

import "../scss/global.scss"
import "semantic-ui-css/semantic.min.css"
import "react-toastify/dist/ReactToastify.css"

import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"

export default function MyApp({ Component, pageProps }) {
  const [auth, setAuth] = useState("")
  const [reloadUser, setReloadUser] = useState(false)
  const [totalProductsCart, setTotalProductsCart] = useState(0)
  const [reloadCart, setReloadCart] = useState(false)
  const router = useRouter()

  useEffect(() => {
    const token = getToken()
    if (token) {
      setAuth({
        token,
        idUser: jwtDecode(token).id,
      })
    } else {
      setAuth(null)
    }
  }, [reloadUser])

  useEffect(() => {
    setTotalProductsCart(countProductCart())
    setReloadCart(false)
  }, [reloadCart, auth])

  const login = token => {
    const toketDecode = jwtDecode(token)
    setToken(token)
    setAuth({
      token,
      idUser: toketDecode.id,
    })
  }

  const logout = () => {
    if (auth) {
      removeToken()
      setAuth(null)
      router.push("/")
    }
  }

  const addProduct = product => {
    const token = getToken()
    if (token) {
      addProductCart(product)
      setReloadCart(true)
    }

    if (!token) {
      toast.warning("para comprar un juego tines que iniciar sesion")
    }
  }

  const authData = useMemo(
    () => ({
      auth,
      login,
      logout,
      setReloadUser,
      setAuth,
    }),
    [auth]
  )

  const removeProduct = product => {
    removeProductCart(product)
    setReloadCart(true)
  }
  const cartData = useMemo(
    () => ({
      productsCard: totalProductsCart,
      addProductCart: product => addProduct(product),
      getProductsCart: getProductsCart,
      removeProductCart: product => removeProduct(product),
      removeAllProductsCart: () => null,
    }),
    [totalProductsCart]
  )

  if (auth === undefined) return null

  return (
    <AuthContext.Provider value={authData}>
      <CartContext.Provider value={cartData}>
        <Component {...pageProps} />
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar
          newestOnTop
          closeOnClick
          rtl={false}
          pauseOnFocusLoss={false}
          draggable
          pauseOnHover
        />
      </CartContext.Provider>
    </AuthContext.Provider>
  )
}
