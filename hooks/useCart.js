import { useContext } from "react"
import CartContext from "../context/CardContext"

export default () => useContext(CartContext)
