import classnames from "classnames"
import { Container } from "semantic-ui-react"
import Header from "../../components/Header/Header"
export default function BasicLayout(x) {
  const { children, className } = x
  //console.log(children.props);
  return (
    <Container
      fluid
      className={classnames("basic-layout", { [className]: className })}
    >
      <Header />
      <Container className="content">{children}</Container>
    </Container>
  )
}
