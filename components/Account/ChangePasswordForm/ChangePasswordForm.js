import { useState } from "react"
import { Form, Button } from "semantic-ui-react"
import useAuth from "../../../hooks/useAuth"

import { useFormik } from "formik"
import * as Yup from "yup"
import { toast } from "react-toastify"
import { updateNameApi } from "../../../api/user"
import LoginForm from "../../Auth/LoginForm"
import { updatePasswordApi } from "../../../api/user"
export default function ChangePasswordForm({ user, logout }) {
  const [loading, setLoading] = useState(false)

  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async formData => {
      setLoading(true)
      const response = await updatePasswordApi(
        user.id,
        formData.password,
        logout
      )

      if (!response) {
        toast.error("Error al actualizar la contrasena")
      } else {
        logout()
      }
    },
  })

  return (
    <div className="change-password-form">
      <h4> Cambiar tu clave </h4>
      <Form onSubmit={formik.handleSubmit}>
        <Form.Group widths="equal">
          <Form.Input
            name="password"
            type="password"
            placeholder="Tu nueva clave"
            onChange={formik.handleChange}
            value={formik.values.password}
            error={formik.errors.password}
          />
          <Form.Input
            name="repeatPassword"
            type="password"
            placeholder="Confirmar clave "
            onChange={formik.handleChange}
            value={formik.values.repeatPassword}
            error={formik.errors.repeatPassword}
          />
        </Form.Group>
        <Button loading={loading} className="submit">
          Actualizar
        </Button>
      </Form>
    </div>
  )
}

function initialValues() {
  return {
    password: "",
    repeatPassword: "",
  }
}

function validationSchema() {
  return {
    password: Yup.string()
      .required(true)
      .oneOf([Yup.ref("repeatPassword")], true),
    repeatPassword: Yup.string()
      .required(true)
      .oneOf([Yup.ref("password")], true),
  }
}
