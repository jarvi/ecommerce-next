import { useState, useEffect } from "react"
import { Form, Button } from "semantic-ui-react"
import { useFormik } from "formik"
import * as Yup from "yup"
import { toast } from "react-toastify"
import { loginApi, resetPasswordApi } from "../../../api/user"
import useAuth from "../../../hooks/useAuth"

export default function LoginForm(props) {
  const { showRegisterForm, onCloseModal } = props
  const [loading, setLoading] = useState(false)

  const { auth, login } = useAuth()

  useEffect(() => {
    console.log(auth)
  }, [auth])

  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async FormData => {
      console.log(FormData)
      setLoading(true)
      const response = await loginApi(FormData)
      if (response?.jwt) {
        console.log("login ok")
        login(response.jwt)
        onCloseModal()
      }
      if (!response?.jwt) {
        toast.error("Email o password incorrectos")
      }
      setLoading(false)
    },
  })

  const resetPassword = _ => {
    formik.setErrors({})

    const validateEmail = Yup.string().email().required()
    console.log(formik.values.identifier)

    if (!validateEmail.isValidSync(formik.values.identifier)) {
      console.log("email incorrecto")
      formik.setErrors({ identifier: true })
    } else {
      console.log(formik.values.identifier)
      console.log("Email valido")
      resetPasswordApi(formik.values.identifier)
    }
  }

  return (
    <Form className="login-form" onSubmit={formik.handleSubmit}>
      <Form.Input
        name="identifier"
        type="text"
        placeholder="Correo elecronico"
        onChange={formik.handleChange}
        error={formik.errors.identifier}
      />
      <Form.Input
        name="password"
        type="password"
        placeholder="Contraseña"
        onChange={formik.handleChange}
        error={formik.errors.password}
      />
      <div className="actions">
        <Button onClick={showRegisterForm} type="button" basic>
          Registrarse
        </Button>
        <div>
          <Button className="submit" type="submit" loading={loading}>
            Entrar
          </Button>
          <Button onClick={resetPassword} type="button">
            Has olvidado la contraseña
          </Button>
        </div>
      </div>
    </Form>
  )
}

function initialValues() {
  return {
    identifier: "",
    password: "",
  }
}

function validationSchema() {
  return {
    identifier: Yup.string().email(true).required(true),
    password: Yup.string().required(true),
  }
}
