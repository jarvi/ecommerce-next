import { useState, useEffect } from "react"
import BasicLayout from "../layouts/BasicLayout"
import { useRouter } from "next/router"
import useAuth from "../hooks/useAuth"
import { getMeApi } from "../api/user"
import ChangeNameForm from "../components/Account/ChangeNameForm"
import ChangeEmailForm from "../components/Account/ChangeEmailForm"
import ChangePasswordForm from "../components/Account/ChangePasswordForm"
import BasicModal from "../components/Modal/BasicModal"
import { Icon } from "semantic-ui-react"
import AddressForm from "../components/Account/AddressForm"
import { Loader } from "semantic-ui-react"

export default function Account() {
  const [user, setUser] = useState('')
  const router = useRouter()
  const { auth, logout, setAuth, setReloadUser } = useAuth()

  useEffect(() => {
    ;(async () => {
      const response = await getMeApi(logout)
      setUser(response || undefined)
      console.log(response)
      setAuth(e => ({
        ...e,
        lastname: response.lastname,
        name: response.name,
        email: response.email,
      }))
    })()
  }, [])

  //if (user == null) return null

  // if (!auth && !user) {
  //   router.replace("/r")
  //   return null
  // }

  return (
    <>
      <BasicLayout className="account">
        {!user && <Loader active> Cargando </Loader>}
        {user && (
          <Configuration
            user={user}
            logout={logout}
            setReloadUser={setReloadUser}
          />
        )}
        {user && <Addresses />}
      </BasicLayout>
    </>
  )
}

function Configuration(props) {
  const { user, logout, setReloadUser } = props

  return (
    <div className="account__configuration">
      <div className="title">Configuracion</div>
      <div className="data">
        <ChangeNameForm
          user={user}
          logout={logout}
          setReloadUser={setReloadUser}
        />
        <ChangeEmailForm
          user={user}
          logout={logout}
          setReloadUser={setReloadUser}
        />
        <ChangePasswordForm user={user} logout={logout} />
      </div>
    </div>
  )
}

function Addresses() {
  const [showModal, setShowModal] = useState(false)
  const [titleModal, setTitleModal] = useState("")
  const [formModal, setFormModal] = useState(null)

  const openModal = () => {
    setTitleModal("Nueva direccion")
    setFormModal(AddressForm)
    setShowModal(true)
  }

  return (
    <div className="account__addresses">
      <div className="title">
        Direcciones
        <Icon name="plus" link onClick={openModal} />
      </div>

      <div className="data">
        <p>lista de direcciones</p>
      </div>
      <BasicModal show={showModal} setShow={setShowModal} title={titleModal}>
        {formModal}
      </BasicModal>
    </div>
  )
}
