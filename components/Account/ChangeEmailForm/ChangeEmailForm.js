import { useState } from "react"
import { Form, Button } from "semantic-ui-react"
import { useFormik } from "formik"
import * as Yup from "yup"
import { toast } from "react-toastify"
import { updateEmailApi } from "../../../api/user"
export default function ChangeEmailForm(props) {

  const { user, logout, setReloadUser } = props
  const [loading, setLoading] = useState(false)
  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async formData => {
      console.log(formData)
      setLoading(true)
      const response = await updateEmailApi(user.id, formData.email, logout)
      if (!response || respose?.statusCode === 400) {
        toast.error("Error al actualizar el email")
      } else {
        
        setReloadUser(x => {
          console.log(x)
          return !x
        })
        //setReloadUser(true)
        toast.success("Email actualzado")
        formik.handleReset()
      }
      setLoading(false)
    },
  })

  return (
    <div className="change-email-form">
      <h4>
        Cambia tu e-mail <span>(Tu email actual es ):{user.email} </span>
      </h4>

      <Form onSubmit={formik.handleSubmit}>
        <Form.Group widths="equal">
          <Form.Input
            name="email"
            placeholder="tu numero e-mail"
            onChange={formik.handleChange}
            values={formik.values.email}
            error={formik.errors.email}
          />
          <Form.Input
            name="repeatEmail"
            placeholder="Confirma tu nuevo e-mail"
            onChange={formik.handleChange}
            values={formik.values.repeatEmail}
            error={formik.errors.repeatEmail}
          />
        </Form.Group>
        <Button loading={loading} type="text" className="submit">
          Actualizar
        </Button>
      </Form>
    </div>
  )
}

function initialValues() {
  return {
    email: "",
    repeatEmail: "",
  }
}

function validationSchema() {
  return {
    email: Yup.string()
      .email(true)
      .required(true)
      .oneOf([Yup.ref("repeatEmail")], "Email no coincide"),
    repeatEmail: Yup.string()
      .email(true)
      .required(true)
      .oneOf([Yup.ref("email")], "Email no coincide"),
  }
}
