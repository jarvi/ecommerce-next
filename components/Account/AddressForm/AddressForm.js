import { useState } from "react"
import { Form, Button } from "semantic-ui-react"

export default function AddressForm() {
  return (
    <Form>
      <Form.Input
        name="title"
        type="text"
        label="Titulo de la direccion"
        placeholder="titulo de la dereccion"
      />
      <Form.Group widths="equal">
        <Form.Input
          name="name"
          type="text"
          label="nombre y apellidos"
          placeholder="nombre y apellidos"
        />
        <Form.Input
          name="address"
          type="text"
          label="direccion"
          placeholder="direccion"
        />
        <Form.Group widths="equal">
          
        </Form.Group>
        <Form.Input
          name="city"
          type="text"
          label="Ciudad"
          placeholder="Ciudad"
        />
        <Form.Input
          name="state"
          type="text"
          label="estado/municipio/parroquia"
          placeholder="estado"
        />
      </Form.Group>
      <Form.Group widths="equal">
        <Form.Input
          name="postalcode"
          type="text"
          label="codigo postal"
          placeholder="codigo postal"
        />
        <Form.Input
          name="numerotlf"
          type="text"
          label="telefono"
          placeholder="telefono"
        />
      </Form.Group>
      <div className="actions">
        <Button className="submit" type="submit">
          Crear direccion
        </Button>
      </div>
    </Form>
  )
}
