import { getToken, hasExpiredToken } from "../api/token"

export async function authFetch(url, params, logout) {
  const token = getToken()

  if (!token) {
    logout()
  }

  if (token) {
    if (hasExpiredToken(token)) {
      logout()
    } else {
      const paramsTemp = {
        ...params,
        headers: {
          ...params?.headers,
          Authorization: `Bearer ${token}`,
        },
      }

      try {
        const response = await fetch(url, paramsTemp)
        const result = response.json()
        return result
      } catch (e) {
        return e
      }
    }
  }
}
