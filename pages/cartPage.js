import { useState, useEffect } from "react"
import BasicLayout from "../layouts/BasicLayout"
import { getGameByUrlApi } from "../api/game"
import useCart from "../hooks/useCart"
import SummaryCartComp from "../components/Cart/SummaryCart"

export default function CartPage() {
  const { getProductsCart } = useCart()
  const products = getProductsCart()
  return !products ? <EmptyCart /> : <FullCart products={products} />
}

function EmptyCart() {
  return (
    <BasicLayout className="empty-cart">
      <h2>No hay productos en el carrito</h2>
    </BasicLayout>
  )
}

function FullCart(props) {
  const { products } = props

  const [productsData, setProductsData] = useState("")
  const [reloadCart, setReloadCart] = useState(false)

  console.log(productsData)

  useEffect(() => {
    ;(async () => {
      const productsTemp = []
      for await (const product of products) {
        const data = await getGameByUrlApi(product)
        productsTemp = [...productsTemp, data]
      }
      setProductsData(productsTemp)
    })()
    setReloadCart(false)
  }, [reloadCart])

  return (
    <BasicLayout className="empty-cart">
      <SummaryCartComp
        productsData={productsData}
        reloadCart={reloadCart}
        setReloadCart={setReloadCart}
      />
    </BasicLayout>
  )
}
