import { useEffect, useState } from "react"
import { Grid, Image, Icon, Button } from "semantic-ui-react"
import { isFavoriteApi } from "../../../api/favorite"
import useAuth from "../../../hooks/useAuth"
import useCart from "../../../hooks/useCart"

export default function HeaderGame(props) {
  const { game, url } = props
  const { poster, title } = game

  return (
    <Grid className="header-game">
      <Grid.Column mobile={16} tablet={6} computer={5}>
        <Image src={poster.url} alt={title} fluid />
      </Grid.Column>
      <Grid.Column mobile={16} tablet={10} computer={11}>
        <Info game={game} />
      </Grid.Column>
    </Grid>
  )
}

function Info(props) {
  const { game } = props

  const { title, summary, price, discount, url } = game

  const [isFavorite, setIsFavorite] = useState(false)
  const { auth, logout } = useAuth()
  const { addProductCart } = useCart()

  useEffect(() => {
    ;(async () => {
      // console.log(auth.idUser)
      // const response = await isFavoriteApi(auth.idUser, game.id, logout)
      // console.log(response)
    })()
  }, [])

  return (
    <>
      <div className="header-game__title">
        <strong>{title}</strong>
        <Icon name="heart outline" link={true} />
      </div>
      <div className="header-game__delivery">Entrega en 24/48h</div>
      <div className="header-game__summary">
        <strong>{summary}</strong>
      </div>

      <div className="header-game__buy">
        <div className="header-game__buy-price">
          <p> Precio de venta al publico:{price} $</p>
          <div className="header-game__buy-price-actions">
            <p> {discount}%</p>
            <p>{price - Math.floor(price * discount) / 100}$</p>
          </div>
        </div>
        <Button
          onClick={_ => addProductCart(url)}
          className="header-game__buy-btn"
        >
          Comprar
        </Button>
      </div>
    </>
  )
}
