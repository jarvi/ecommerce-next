//import { Button, Icon } from "semantic-ui-react"
import { useState, useEffect } from "react"
import BacicLayout from "../layouts/BasicLayout"
import { getLastGamesApi } from "../api/game"
import ListGames from "../components/ListGames"
import Seo from "../components/Seo"

import { Loader } from "semantic-ui-react"
export default function Home() {
  const [games, setGames] = useState("")

  useEffect(() => {
    ;(async () => {
      const response = await getLastGamesApi(10)

      if (response.length > 0) setGames(response)
    })()
  }, [])

  return (
    <BacicLayout className="home">
      <Seo title="Compra tu juego" />
      {!games && <Loader active> Cargando juegos </Loader>}
      {games && games.length === 0 && (
        <div>
          <h3> No hay juegos</h3>
        </div>
      )}
      {games.length > 0 && <ListGames games={games} />}
    </BacicLayout>
  )
}
