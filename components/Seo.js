import React from "react"

import Head from "next/head"

export default function Seo(props) {
  const { title, description } = props

  return (
    
    <Head>
      <title>{title}</title>
      <meta name="random" property="description" content={description} />
      <meta property="og:title" content="Ecommerce-jarvis-game" key="title" />
    </Head>

  )
}

Seo.defaultProps = {
  title: "Gaming - tus juegos favoritos",
  description: "Tus juegos para steam, PlayStation",
}
