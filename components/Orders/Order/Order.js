import { useState, useEffect } from "react"
import Link from "next/link"
import { Image, Icon } from "semantic-ui-react"
import BasicModal from "../../Modal/BasicModal"

export default function Order(props) {
  const { order } = props
  const { juego: game, totalPayment } = order
  const { title, poster, url } = game
  console.log("en el componente Order")
  return (
    <>
      <div className="order">
        <div className="order__info">
          <Link href={`/${url}`}>
            <a>
              <Image src={poster.url} alt={title} />
            </a>
          </Link>
        </div>
      </div>
    </>
  )
}
