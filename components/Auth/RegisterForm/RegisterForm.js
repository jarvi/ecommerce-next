import { useState } from "react"
import { Form, Button } from "semantic-ui-react"
import { useFormik } from "formik"
import * as Yup from "yup"
import { registerApi } from "../../../api/user"

import { toast } from "react-toastify"

export default function RegisterForm(props) {
  const [loadieng, setLoadieng] = useState(false)
  const { showLoginForm } = props
  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSquema()),
    onSubmit: async formData => {
      setLoadieng(true)
      const response = await registerApi(formData)
      console.log(response)
      if (response?.jwt) {
        showLoginForm()
      }
      if (!response.jwt) {
        toast.error("error al registrar")
      }
      setLoadieng(false)
    },
  })

  return (
    <Form className="login-form" onSubmit={formik.handleSubmit}>
      <Form.Input
        name="name"
        type="text"
        placeholder="Nombre"
        onChange={formik.handleChange}
        error={formik.errors.name}
      />
      <Form.Input
        name="lastname"
        type="text"
        placeholder="Apellido"
        onChange={formik.handleChange}
        error={formik.errors.lastname}
      />
      <Form.Input
        name="username"
        type="text"
        placeholder="Nombre de usuario"
        onChange={formik.handleChange}
        error={formik.errors.username}
      />
      <Form.Input
        name="email"
        type="text"
        placeholder="Correo electronico"
        onChange={formik.handleChange}
        error={formik.errors.email}
      />
      <Form.Input
        name="password"
        type="password"
        placeholder="Contraseña"
        onChange={formik.handleChange}
        error={formik.errors.password}
      />
      <div className="actions">
        <Button onClick={showLoginForm} type="button" basic>
          Iniciar sesion
        </Button>
        <Button type="submit" className="submit" loading={loadieng}>
          registrar
        </Button>
      </div>
    </Form>
  )
}

function initialValues() {
  return {
    name: "",
    lastname: "",
    username: "",
    email: "",
  }
}

function validationSquema() {
  return {
    name: Yup.string().required(true),
    lastname: Yup.string().required("El apellido es requerido"),
    username: Yup.string().required(true),
    email: Yup.string().email(true).required(true),
    password: Yup.string().required(true),
  }
}
