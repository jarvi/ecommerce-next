import React, { useEffect, useState } from "react"
import { Table, Image, Icon } from "semantic-ui-react"
import useCart from "../../../hooks/useCart"

export default function SummaryCartComp(props) {
  const { productsData, reloadCart, setReloadCart } = props
  const [totalPrice, setTotalPrice] = useState(0)

  const { removeProductCart } = useCart()

  useEffect(
    _ => {
      console.log("en useEfect")
      let price = 0
      if (typeof productsData === "object") {
        productsData.forEach(product => {
          price += product.price
        })
        setTotalPrice(price)
      }
    },
    [reloadCart, productsData]
  )
  const removeProduct = product => {
    removeProductCart(product)
    setReloadCart(true)
  }

  return (
    <div className="summary-cart">
      <div className="title"> Resumen del carrito </div>
      <div className="data">
        <Table celled structured>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Producto</Table.HeaderCell>
              <Table.HeaderCell>Plataforma</Table.HeaderCell>
              <Table.HeaderCell>Entrega</Table.HeaderCell>
              <Table.HeaderCell>Precio</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {productsData &&
              productsData.map(product => (
                <Table.Row key={product.id} className="summary-cart__product">
                  <Table.Cell>
                    <Icon
                      name="close"
                      link
                      onClick={() => removeProduct(product.url)}
                    />
                    <Image
                      className="img"
                      src={product.poster.url}
                      alt={product.title}
                    />
                    {product.title}
                  </Table.Cell>
                  <Table.Cell>{product.platform.title}</Table.Cell>
                  <Table.Cell>{"Inmediata"}</Table.Cell>
                  <Table.Cell>{product.price}$</Table.Cell>
                </Table.Row>
              ))}

            <Table.Row className="summary-cart__resume">
              <Table.Cell className="clear" />
              <Table.Cell colSpan="2"> Total: </Table.Cell>
              <Table.Cell className="total-price"> {totalPrice}$</Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>
      </div>
    </div>
  )
}
