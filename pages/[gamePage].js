import React, { useState, useEffect } from "react"
import BasicLayout from "../layouts/BasicLayout"
import { useRouter } from "next/router"
import { getGameByUrlApi } from "../api/game"
import HeaderGame from "../components/Game/HeaderGame"
import TabsGame from "../components/Game/TabsGame"
import { Loader } from "semantic-ui-react"
import useCart from "../hooks/useCart"

export default function GamePage({ game }) {
  //const [game, setGame] = useState("")
  const { query, push } = useRouter()
  console.log(game)
  // useEffect(() => {
  //   (async () => {
  //     const response = await getGameByUrlApi(query.gamesPage)
  //     setGame(response)
  //   })()
  //   console.log("%cen use efect", "color:green")
  // }, [query])

  return (
    <BasicLayout className="game">
      {!game && <Loader active> Cargando </Loader>}
      {game && game.length === 0 && <h2> Juego no encontrado</h2>}
      {game && game.id && <HeaderGame game={game} />}
      {game && game.id && <TabsGame game={game} />}
    </BasicLayout>
  )
}

export async function getServerSideProps(context) {
  //console.log(context.params.gamePage)
  const response = await getGameByUrlApi(context.params.gamePage)
  return {
    props: { game: response }, 
  }
}
