import React, { useState, useEffect } from "react"
import { Loader } from "semantic-ui-react"
import BasicLayout from "../layouts/BasicLayout"
import { searchGameApi } from "../api/game"
import ListGame from "../components/ListGames"
import { useRouter } from "next/router"

export default function SearchPage() {
  const [games, setGames] = useState('')
  const { query } = useRouter()

  useEffect(() => {
    document.querySelector("#search-game").focus()
  }, [])

  useEffect(() => {
    ;(async () => {
      if (query.query.length > 0) {
        const response = await searchGameApi(query.query)
        console.log(response)
        setGames(response)
      } else {
        setGames([])
      }
    })()
  }, [query])

  return (
    <BasicLayout className="search">
      {!games && <Loader active> Cargando </Loader>}
      {games && games.length === 0 && <h3>no hay juegos para mostrar</h3>}
      {games.length > 0 && <ListGame games={games}></ListGame>}
    </BasicLayout>
  )
}
