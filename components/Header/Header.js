import TopBar from "./TopBar"
import Menu from "./Menu"
export default function Header() {
  return (
    <header className="header">
      <TopBar />
      <Menu />
    </header>
  )
}
