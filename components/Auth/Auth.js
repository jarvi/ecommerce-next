import { useState } from "react"
import LoginForm from "./LoginForm"
import RegisterForm from "./RegisterForm"

export default function Auth(props) {
  
  const [showLogin, setShowLogin] = useState(true)
  const { onCloseModal, setTitleModal } = props

  const showLoginForm = () => {
    setTitleModal("Inicio de sesion")
    setShowLogin(true)
  }

  const showRegisterForm = () => {
    setTitleModal("crear usuario")
    setShowLogin(false)
  }

  return showLogin ? (
    <LoginForm
      showRegisterForm={showRegisterForm}
      onCloseModal={onCloseModal}
    />
  ) : (
    <RegisterForm showLoginForm={showLoginForm} />
  )
}
