import { BASE_PATH } from "../utils/constants"
import { authFetch } from "../utils/fetch"
export async function registerApi(formData) {
  try {
    const url = `${BASE_PATH}/auth/local/register`
    const params = {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(formData),
    }
    const response = await fetch(url, params)
    return response.json()
  } catch (error) {
    console.log(error)
    return null
  }
}

export async function loginApi(data) {
  try {
    const url = `${BASE_PATH}/auth/local`
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    }
    const response = await fetch(url, params)
    return response.json()
  } catch (e) {
    console.log(e)
  }
}

export async function resetPasswordApi(email) {
  
  try {
    const url = `${BASE_PATH}/auth/forgot-password`
    const params = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email }),
    }
    
    const response = await fetch(url, params)

    return response.json()
  } catch (e) {
    console.log(e)
    return null
  }
}

export async function getMeApi(logout) {
  try {
    const url = `${BASE_PATH}/users/me`
    const result = await authFetch(url, null, logout)
    return result ? result : null
  } catch (e) {
    return null
  }
}

export async function updateNameApi(idUser, data, logout) {
  try {
    const url = `${BASE_PATH}/users/${idUser}`
    const params = {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    }

    const result = await authFetch(url, params, logout)
    return result
  } catch (e) {
    console.log(e)
    return e
  }
}

export async function updateEmailApi(idUser, email, logout) {
  try {
    const url = `${BASE_PATH}/users/${idUser}`
    const params = {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email }),
    }

    const result = await authFetch(url, params, logout)
    return result
  } catch (e) {
    console.log(e)
    return null
  }
}

export async function updatePasswordApi(idUser, password, logout) {
  try {
    const url = `${BASE_PATH}/users/${idUser}`
    const params = {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ password }),
    }

    const result = await authFetch(url, params, logout)
    return result
  } catch (e) {
    console.log(e)
    return null
  }
}
