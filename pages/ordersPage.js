import { useState, useEffect } from "react"
import { Grid } from "semantic-ui-react"
import BasicLayout from "../layouts/BasicLayout"
import { getOrdersApi } from "../api/order"
import useAuth from "../hooks/useAuth"
import Order from "../components/Orders/Order"
import { Loader } from "semantic-ui-react"
export default function PageOrders() {
  const [orders, setOrders] = useState("")
  const { auth, logout } = useAuth()

  useEffect(() => {

    (async () => {
      
      if (auth.idUser) {
        const response = await getOrdersApi(auth.idUser, logout)
        setOrders(response || [])
        
      }
    })()
  }, [])
  
  return (
    <BasicLayout className="orders">
      {!orders && <Loader active> Cargando </Loader>}
      {orders && orders.length > 0 && 
        <div className="orders__block">
          <div className="title">Mis pedidos </div>
          <div className="data">
            {orders.length === 0 ? (
              <h2 style={{ textAlign: "center" }}>
                No has realizado una compra
              </h2>
            ) : (
              <OrderList orders={orders} />
            )}
          </div>
        </div>
      }
    </BasicLayout>
  )
}

function OrderList(props) {
  const { orders } = props
  console.log(orders)
  return (
    <Grid>
      {orders.map(order => (
        <Grid.Column key={order.id} mobile={16} table={6} computer={8}>
          <Order order={order} />
        </Grid.Column>
      ))}
    </Grid>
  )
}

//users_permissions_user
