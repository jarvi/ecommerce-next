import { useState } from "react"
import { Form, Button } from "semantic-ui-react"
import useAuth from "../../../hooks/useAuth"

import { useFormik } from "formik"
import * as Yup from "yup"
import { toast } from "react-toastify"
import { updateNameApi } from "../../../api/user"

export default function ChangeNameForm({ user, logout, setReloadUser }) {
  const { auth } = useAuth()
  const [loading, setLoading] = useState(false)
  const formik = useFormik({
    initialValues: initialValues(user.name, user.lastname),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async formData => {
      setLoading(true)
      const response = await updateNameApi(user.id, formData, logout)

      console.log(response)

      if (!response) {
        toast.error("Error al actualizar")
      } else {
        // console.log("nombre actualizado")
        // toast.success('nombre y apellido actualizado')
        setReloadUser(true)
        //setReloadUser(z => !z)
      }
      setLoading(false)
    },
  })

  return (
    <div className="change-name-form">
      <h4>cambia tu nombre y apellido</h4>
      <Form onSubmit={formik.handleSubmit}>
        <Form.Group widths="equal">
          <Form.Input
            onChange={formik.handleChange}
            name="name"
            placeholder="Tu nuevo nombre "
            value={formik.values.name}
            error={formik.errors.name}
          />
          <Form.Input
            onChange={formik.handleChange}
            name="lastname"
            placeholder="Tu nuevo apellido "
            value={formik.values.lastname}
            error={formik.errors.lastname}
          />
        </Form.Group>
        <Button loading={loading} className="submit">
          Actualizar
        </Button>
      </Form>
    </div>
  )
}

function initialValues(name, lastname) {
  return {
    name: name || "",
    lastname: lastname || "",
  }

  return {
    name,
    lastname,
  }
}

function validationSchema() {
  return {
    name: Yup.string().required(true),
    lastname: Yup.string().required(true),
  }
}
